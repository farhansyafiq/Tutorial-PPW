from django.shortcuts import render
from datetime import date

# Enter your name here
mhs_name = 'Teuku Muhammad Farhan Syafiq' # TODO Implement this

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age' : calculate_age(1999)}
    return render(request, 'index.html', response)

# TODO Implement this to complete last checklist
def calculate_age(birth_year):
	today = date.today().year
	return today - birth_year

